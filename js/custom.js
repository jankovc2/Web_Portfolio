/*jslint browser: true*/
/*global $, WOW, document*/
$(document).ready(function () {
    new WOW().init();
});


// Smooth Scrolling
$(function () {

    $("a.smooth-scroll").click(function (event) {
        event.preventDefault;

        var section = $(this).attr("href");

        $('html, body').animate({
            scrollTop: $(section).offset().top
        }, 3000);

    });

});
